﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BloqCert.Interfaces;
using Foundation;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(BloqCert.iOS.Implementation.KeyboardDismissService))]

namespace BloqCert.iOS.Implementation
{
    class KeyboardDismissService : IKeyboardDismissService
    {

        public void DismissKeyboard()
        {

            UIApplication.SharedApplication.InvokeOnMainThread(() =>
            {
                var window = UIApplication.SharedApplication.KeyWindow;
                var vc = window.RootViewController;
                while (vc.PresentedViewController != null)
                {
                    vc = vc.PresentedViewController;
                }

                vc.View.EndEditing(true);
            });
        }

    }
}