﻿using System;

using BloqCert.Android.Implementation;
using BloqCert.UserControls;

using DroidGraphics = Android.Graphics;
using Android.Graphics.Drawables;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Content;

[assembly: ExportRenderer(typeof(CustomDatePicker), typeof(CustomDatePickerRenderer))]
namespace BloqCert.Android.Implementation
{
    public class CustomDatePickerRenderer : DatePickerRenderer
    {
        Context context;
        BloqCert.UserControls.CustomDatePicker control;

        public CustomDatePickerRenderer(Context context) : base(context)
        {

            this.context = context;
        }


        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                control = e.NewElement as CustomDatePicker;
            }

            this.Control.SetTextColor(control.TextColor.ToAndroid());
            this.Control.SetBackgroundColor(control.BackgroundColor.ToAndroid());
            this.Control.SetPadding((int)control.Padding.Left, (int)control.Padding.Top, (int)control.Padding.Right, (int)control.Padding.Bottom);

            GradientDrawable gd = new GradientDrawable();
            gd.SetCornerRadius((float)control.CornerRadius); //increase or decrease to changes the corner look
            gd.SetColor(DroidGraphics.Color.Transparent);
            gd.SetStroke((int)control.BorderWidth, control.BorderColor.ToAndroid());

            this.Control.SetBackgroundDrawable(gd);

            CustomDatePicker element = Element as CustomDatePicker;

            if (!string.IsNullOrWhiteSpace(element.WatermarkText) && control.Date == DateTime.MinValue)
            {
                Control.Text = element.WatermarkText;
            }

            // control.PropertyChanged += Control_PropertyChanged;

            this.Control.TextChanged += (sender, arg) => {

                var selectedDate = arg.Text.ToString();

                if (selectedDate == element.WatermarkText)
                {
                    Control.Text = DateTime.Now.ToString("dd/MM/yyyy");
                }

            };
        }

        private void Control_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            



        }
    }
}