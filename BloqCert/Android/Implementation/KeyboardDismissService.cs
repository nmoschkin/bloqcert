﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using BloqCert.Interfaces;
using Plugin.CurrentActivity;
using Xamarin.Forms;

[assembly: Dependency(typeof(BloqCert.Android.Implementation.KeyboardDismissService))]
namespace BloqCert.Android.Implementation
{
    public class KeyboardDismissService : IKeyboardDismissService
    {

        private static Context _context;

        public static void Init(Context context)
        {
            _context = context;
        }

        public void DismissKeyboard()
        {
            InputMethodManager imm = _context.GetSystemService(Context.InputMethodService) as InputMethodManager;

            var act = _context as MainActivity;

            imm.HideSoftInputFromWindow(
                act.Window.DecorView.WindowToken, HideSoftInputFlags.None);

            act.Window.DecorView.ClearFocus();

        }

    }
}