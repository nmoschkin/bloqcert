using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using BloqCert.Views.LoginScreen;
using BloqCert.Models;
using BloqCert.Views.MainScreen;
using System.Globalization;
using System.Threading;
using BloqCert.Localization.Interfaces;
using BloqCert.Localization.Resources;
using BloqCert.Localization;
using Xamarin.Essentials;
using Microsoft.AppCenter.Crashes;
using BloqCert.Views.AppScreen;

namespace BloqCert
{
    public partial class App : Application, ICultureManager
    {

        private UserInfo user = new UserInfo();

        public UserInfo User
        {
            get => user;
            set
            {
                user = value;
            }
        }

        public App()
        {
            InitializeComponent();

            InitLanguage();


            //user.Firstname = "Nathan";
            //user.Lastname = "Moschkin";
            //user.UserId = "123XYZ";

            //user.Username = "nmoschkin";
            //user.OrganizationID = "PreciseSoft";

            //MainPage = new NavigationPage(new MainScreen(Settings.Role));

            //if (Settings.IsLoggedIn == true)
            //{
            //    MainPage = new MainScreen(ViewModels.CertRole.FDA);
            //}
            //else
            //{

            //    MainPage = new AppScreen();
            MainPage = new LoginScreen();
            //}

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }


        #region Internationalization

        private CultureInfo mci;
        private CultureInfo oscult;

        public FlowDirection FlowDirection
        {
            get
            {
                switch (mci.TwoLetterISOLanguageName)
                {
                    case "ar":
                    case "he":
                        return FlowDirection.RightToLeft;

                    default:
                        return FlowDirection.LeftToRight;
                }
            }
        }

        public void InitLanguage()
        {

            CultureInfo ci;

            if (Device.RuntimePlatform == Device.iOS || Device.RuntimePlatform == Device.Android)
            {
                ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            }
            else
            {
                ci = CultureInfo.CurrentCulture;
            }

            // For the lifetime of the program
            oscult = ci;

            if (VersionTracking.IsFirstLaunchEver)
            { // see if we support this language else fall back to default "en"
                Languages supportedLangs = new Languages();
                if (!supportedLangs.Any(l => l.Code == ci.TwoLetterISOLanguageName))
                { // we don't then
                    ActiveCulture = Settings.UserCulture;
                }
                else
                {
                    ActiveCulture = ci;
                    Settings.Language = ci.TwoLetterISOLanguageName;

                    ConfigureAppForCulture(ci);
                }
            }
            else
            {
                ActiveCulture = Settings.UserCulture;
                if (ActiveCulture == null)
                {
                    ResetCulture();
                }

            }

            // ActiveCulture = oscult = ci = new CultureInfo("fr");



        }

        public CultureInfo ActiveCulture
        {
            get
            {
                return mci;
            }
            set
            {
                mci = value;

                AppResources.Culture = mci; // set the RESX for resource localization

                CultureInfo.DefaultThreadCurrentCulture = mci;
                CultureInfo.DefaultThreadCurrentUICulture = mci;

                CultureInfo.CurrentCulture = mci;

                Thread.CurrentThread.CurrentUICulture = mci;
                Thread.CurrentThread.CurrentCulture = mci;

            }
        }

        public void ConfigureAppForCulture(CultureInfo cult)
        {
            switch (cult.TwoLetterISOLanguageName)
            {
                case "en":
                    Settings.IsMetric = false;
                    break;
                case "fr":
                    Settings.IsMetric = true;
                    //Settings.ClockType = HourFormat.Hour24;
                    break;
                default:
                    Settings.IsMetric = true;
                    break;
            }
        }

        public void ResetCulture()
        {
            CultureInfo ci = oscult;

            ConfigureAppForCulture(ci);
            Settings.UserCulture = ci;
        }
        #endregion

        internal static void TrackError(Exception ex, System.Collections.Generic.IDictionary<string, string> properties = null)
        {
            if (Device.RuntimePlatform == Device.Android || Device.RuntimePlatform == Device.iOS)
            {
#pragma warning disable CS0618
                Crashes.TrackError(ex, properties);
#pragma warning restore
            }


        }


    }
}
