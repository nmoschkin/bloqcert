﻿namespace BloqCert.Views.LoginScreen
{
    public interface INavigationHandler
    {
        void LoadView(ViewType viewType);
    }
}