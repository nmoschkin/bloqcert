﻿using BloqCert.Helpers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Telerik.XamarinForms.Common;
using Xamarin.Forms;

namespace BloqCert.Views.LoginScreen
{
    public class LoginPageViewModel : NotifyPropertyChangedBase
    {
        private string username;
        private string email;
        private string password;
        private string password2;
        private string role;
        private bool isFingerprintEnabled;

        public LoginPageViewModel()
        {
            this.GoToViewCommand = new Command(this.LoadView);
            this.LoginCommand = new Command(this.ExecuteLogin);
            this.ResetPasswordCommand = new Command(this.SubmitPasswordReset);
        }

        [JsonProperty("username")]
        public string Username
        {
            get => this.username;
            set
            {
                if (this.username == value)
                {
                    return;
                }

                this.username = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("email")]
        public string Email
        {
            get => this.email;
            set
            {
                if (this.email == value)
                {
                    return;
                }

                this.email = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("password")]
        public string Password
        {
            get => this.password;
            set
            {
                if (this.password == value)
                {
                    return;
                }

                this.password = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore()]
        public string RetypePassword
        {
            get => this.password2;
            set
            {
                if (this.password2 == value)
                {
                    return;
                }

                this.password2 = value;
                OnPropertyChanged();
            }
        }


        [JsonIgnore()]
        public string Role
        {
            get => this.role;
            set
            {
                if (this.role == value)
                {
                    return;
                }

                this.role = value;
                OnPropertyChanged();
            }
        }



        [JsonIgnore()]
        public bool IsFingerprintEnabled
        {
            get => this.isFingerprintEnabled;
            set
            {
                if (this.isFingerprintEnabled == value)
                {
                    return;
                }

                this.isFingerprintEnabled = value;
                OnPropertyChanged();
            }
        }

        public Command GoToViewCommand { get; set; }

        public Command LoginCommand { get; set; }

        public Command ResetPasswordCommand { get; set; }

        public INavigationHandler NavigationHandler { private get; set; }

        private void LoadView(object viewType)
        {
            this.NavigationHandler.LoadView((ViewType)viewType);
        }

        private async void ExecuteLogin(object obj)
        {
            if (this.IsFingerprintEnabled)
            {
                await Task.Run(async () =>
                {
                    // !! IMPORTANT  Follow setup instructions in README.txt !!

                    var result = await Plugin.Fingerprint.CrossFingerprint.Current.AuthenticateAsync("Login with fingerprint");

                    // Example error message: "Canceled by user."
                    if (!string.IsNullOrEmpty(result.ErrorMessage))
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await Application.Current.MainPage.DisplayAlert("Fingerprint Login Error", result.ErrorMessage, "ok");
                        });
                        
                        return;
                    }
                    
                    // Success can be determined by checking result.Authenticated directly
                    if (result.Authenticated)
                    {
                        // Allowed
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await Application.Current.MainPage.DisplayAlert("Fingerprint Login", "You have successfully logged in with your fingerprint", "ok");
                        });
                    }
                    else
                    {
                        // Denied
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            // Simulator Note: If you get a "NotAvailable" status message, enable TouchID (https://github.com/smstuebe/xamarin-fingerprint#testing-on-simulators)
                            await Application.Current.MainPage.DisplayAlert("Fingerprint Login",
                                $"You were not logged in. Status: {result.Status}. Make sure you've enabled permissions or simulator/emulator fingerprint features.",
                                "ok");
                        });
                    }
                });
            }
            else
            {
                switch ((LoginType)obj)
                {
                    case LoginType.Normal:

                        var resp = await NetHelper.Login(this);

                        if (resp?.Success ?? false)
                        {

                            ((App)App.Current).User.UserId = Username;
                            ((App)App.Current).User.Username = Username;

                            if (string.IsNullOrEmpty(resp.Role))
                            {


                                if (username.ToLower().IndexOf("exporter") >= 0)
                                {
                                    App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.Exporter));
                                }
                                else if (username.ToLower().IndexOf("fda") >= 0)
                                {
                                    App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.FDA));
                                }
                                else if (username.ToLower().IndexOf("admin") >= 0)
                                {
                                    App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.Admin));
                                }
                                else
                                {
                                    App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.Exporter));
                                }
                            }
                            else
                            {
                                switch(resp.Role.ToLower())
                                {
                                    case "fda":
                                        App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.FDA));

                                        break;

                                    case "importer":
                                        App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.Importer));

                                        break;

                                    case "foreign":
                                    case "cbp":
                                        App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.CBP));

                                        break;

                                    case "admin":
                                        App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.Admin));

                                        break;

                                    case "exporter":
                                    default:

                                        App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.Exporter));

                                        break;
                                }
                            }


                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("User Login",
                                $"You were not logged in. Invalid username or password.",
                                "OK");
                        }

                        break;
                    case LoginType.SignUp:


                        if (Password != RetypePassword)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", "Passwords don't match", "Ok");
                            return;
                        }

                        if (string.IsNullOrEmpty(Role))
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", "Must have role", "Ok");

                        }

                        Role = role.ToLower();

                        var b = await NetHelper.CreateUser(username, password, role);

                        if (b)
                        {

                            await Application.Current.MainPage.DisplayAlert("Success", "User Created", "Ok");

                            Username = "";
                            Password = "";
                            RetypePassword = "";

                            App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(ViewModels.CertRole.Admin));


                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", "Cannot create user", "Ok");

                        }

                        break;
                    case LoginType.PasswordReset:
                        // Use Email property for reset request
                        await Application.Current.MainPage.DisplayAlert("Password Reset Requested", $"Password reset for: Email:{Email}", "ok");
                        break;
                }

            }
        }

        private void SubmitPasswordReset()
        {
            // Use the Email property
            Application.Current.MainPage.DisplayAlert("Password Reset", $"You have requested a password reset for: {Email}", "ok");
        }
    }
}