﻿The LoginScreenWithFingerprint uses Plugin.Fingerprint to enable cross platform biometric sensor access

NuGet https://www.nuget.org/packages/Plugin.Fingerprint/
Home https://github.com/smstuebe/xamarin-fingerprint
License https://github.com/smstuebe/xamarin-fingerprint/blob/master/LICENSE


------ Quick Start ----- 

Fingerprint API access requires certain permissions enabled https://github.com/smstuebe/xamarin-fingerprint#setup


----- Details -----

Support Platform Versions: https://github.com/smstuebe/xamarin-fingerprint#support
Setup and Permissions: https://github.com/smstuebe/xamarin-fingerprint#setup
Usage: https://github.com/smstuebe/xamarin-fingerprint#usage
Testing on Simulators: https://github.com/smstuebe/xamarin-fingerprint#testing-on-simulators