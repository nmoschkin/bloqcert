﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BloqCert.Views.LoginScreen.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView : ContentView
    {
        public LoginView ()
        {

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent ();
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width > height)
            {
                Username.HorizontalOptions = LayoutOptions.Center;
                Password.HorizontalOptions = LayoutOptions.Center;
                LoginBtn.HorizontalOptions = LayoutOptions.Center;

                Username.WidthRequest = (width / 2);
                Password.WidthRequest = (width / 2);
                LoginBtn.WidthRequest = (width / 2);

            }
            else
            {
                Username.HorizontalOptions = LayoutOptions.CenterAndExpand;
                Password.HorizontalOptions = LayoutOptions.CenterAndExpand;
                LoginBtn.HorizontalOptions = LayoutOptions.CenterAndExpand;

                Username.WidthRequest = (width / 3) * 2;
                Password.WidthRequest = (width / 3) * 2;
                LoginBtn.WidthRequest = (width / 3) * 2;

            }

        }
    }
}