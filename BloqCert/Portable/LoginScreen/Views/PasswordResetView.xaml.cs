﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BloqCert.Views.LoginScreen.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PasswordResetView : ContentView
	{
		public PasswordResetView ()
		{
			InitializeComponent ();
		}
	}
}