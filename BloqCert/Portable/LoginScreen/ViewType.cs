﻿namespace BloqCert.Views.LoginScreen
{
    public enum ViewType
    {
        LoginView,
        SignUpView,
        PasswordResetView
    }
}