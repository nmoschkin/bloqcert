﻿namespace BloqCert.Views.LoginScreen
{
    public enum LoginType
    {
        Normal,
        SignUp,
        PasswordReset
    }
}
