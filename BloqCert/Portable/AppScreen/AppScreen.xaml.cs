﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using BloqCert.Models;
using BloqCert.ViewModels;
using BloqCert.Helpers;
using BloqCert.Views.MainScreen;
using BloqCert.Localization.Resources;
using Telerik.XamarinForms.Primitives;
using DataTools.Text;

namespace BloqCert.Views.AppScreen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppScreen : ContentPage
    {

        private ECertViewModel vm;

        public AppScreen()
        {
            vm = new ECertViewModel();
            if (string.IsNullOrEmpty(vm.Status))
            {
                vm.Status = "Requested";
            }
            // vm.Source.AssignNewId();

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            BuildTabs();

            BindingContext = vm;

        }


        private void BuildTabs()
        {
            ViewArea.Items.Clear();

            TabViewItem item;

            int x = 1;

            item = new TabViewItem();
            item.Content = new ExporterView(x++, AppResources.ExporterInfo, vm.ApplicationId) { BindingContext = vm };
            item.Header.IsVisible = false;

            ViewArea.Items.Add(item);

            item = new TabViewItem();
            item.Content = new ContactInfoView(x++, AppResources.ProducerInfo, vm.ApplicationId) { BindingContext = vm.Facility };
            item.Header.IsVisible = false;

            ViewArea.Items.Add(item);

            //item = new TabViewItem();
            //item.Content = new ContactInfoView(2, AppResources.ExporterInfo) { BindingContext = vm };
            //item.Header.IsVisible = false;

            //ViewArea.Items.Add(item);

            item = new TabViewItem();
            item.Content = new ShipmentView(x++, AppResources.ShipmentInfo, vm.ApplicationId) { BindingContext = vm.Shipment };
            item.Header.IsVisible = false;

            ViewArea.Items.Add(item);

            item = new TabViewItem();
            item.Content = new ProductView(x++, AppResources.ProductInfo, vm.ApplicationId) { BindingContext = vm.Product };
            item.Header.IsVisible = false;

            ViewArea.Items.Add(item);


            SetButtonText();
        }

        private void SetButtonText()
        {
            if (ViewArea.SelectedItem == ViewArea.Items.First())
            {
                BackBtn.Text = AppResources.Cancel;
            }
            else
            {
                BackBtn.Text = AppResources.Back;
            }

            if (ViewArea.SelectedItem == ViewArea.Items.Last())
            {
                NextBtn.Text = AppResources.Submit;
            }
            else
            {
                NextBtn.Text = AppResources.Next;
            }

            
        }

        private async void BackBtn_Clicked(object sender, EventArgs e)
        {
            var db = new DatabaseHelper();

            TextTools.ClassTrimTool(vm);
            TextTools.ClassTrimTool(vm.Facility);
            TextTools.ClassTrimTool(vm.Product);
            TextTools.ClassTrimTool(vm.Shipment);

            vm.Status = "Requested";

            Settings.ExporterId = vm.ExporterId;

            await db.SaveECertAsync(vm.Source);

            if (BackBtn.Text == AppResources.Cancel)
            {
                try
                {
                    await Navigation.PopAsync();
                }
                catch
                {
                    App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(CertRole.CBP));
                }
            }
            else
            {
                int idx = ViewArea.Items.IndexOf((TabViewItem)ViewArea.SelectedItem);
                idx--;

                ViewArea.SelectedItem = ViewArea.Items[idx];
                SetButtonText();
            }
        }

        private async void NextBtn_Clicked(object sender, EventArgs e)
        {
            var db = new DatabaseHelper();

            TextTools.ClassTrimTool(vm);
            TextTools.ClassTrimTool(vm.Facility);
            TextTools.ClassTrimTool(vm.Product);
            TextTools.ClassTrimTool(vm.Shipment);

            vm.Status = "Requested";
            Settings.ExporterId = vm.ExporterId;

            await db.SaveECertAsync(vm.Source);

            if (NextBtn.Text == AppResources.Submit)
            {

                var res = await NetHelper.SendApplication(vm.Source);

                if (res != null)
                {
                    vm.TransactionId = res.TransactionId;
                    await db.SaveECertAsync(vm.Source);
                }

                try
                {
                    await Navigation.PopAsync();
                }
                catch
                {
                    App.Current.MainPage = new NavigationPage(new BloqCert.Views.MainScreen.MainScreen(CertRole.CBP));
                }
            }
            else
            {
                int idx = ViewArea.Items.IndexOf((TabViewItem)ViewArea.SelectedItem);
                idx++;

                ViewArea.SelectedItem = ViewArea.Items[idx];
                SetButtonText();
            }

        }

        private void ClearBtn_Clicked(object sender, EventArgs e)
        {


            var src = new ECert(true);

            Settings.LastSaveCert = null;

            vm = new ECertViewModel(src);

            vm.Status = "Requested";

            BindingContext = vm;
            BuildTabs();

        }

        private async void NewIdBtn_Clicked(object sender, EventArgs e)
        {
            var db = new DatabaseHelper();

            vm.AssignNewId();
            await db.SaveECertAsync(vm.Source);

            BuildTabs();

        }
    }
}