﻿using BloqCert.Localization.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BloqCert.Views.AppScreen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductView : ContentView
    {
        public ProductView()
        {
            InitializeComponent();
        }

        public ProductView(int sectionId, string section, string applicationNumber)
        {

            InitializeComponent();

            SectionText.Text = string.Format(AppResources.SectionX, sectionId) + ": " +
                                section;

            ApplicationNumber.Text = applicationNumber;
        }

    }
}