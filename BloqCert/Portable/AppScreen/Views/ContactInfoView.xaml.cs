﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BloqCert.Localization.Resources;

namespace BloqCert.Views.AppScreen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactInfoView : ContentView
    {
        public ContactInfoView()
        {
            InitializeComponent();
        }

        public ContactInfoView(int sectionId, string section, string applicationNumber)
        {

            InitializeComponent();

            SectionText.Text = string.Format(AppResources.SectionX, sectionId) + ": " +
                                section;

            ApplicationNumber.Text = applicationNumber;

        }

    }
}