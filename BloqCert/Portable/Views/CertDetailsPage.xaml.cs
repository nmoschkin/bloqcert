﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using BloqCert.Models;
using BloqCert.ViewModels;
using BloqCert.Localization.Resources;
using System.Collections.Specialized;
using BloqCert.Helpers;

namespace BloqCert.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CertDetailsPage : ContentPage
    {

        private ECertViewModel vm;

        public CertDetailsPage()
        {

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();


            vm = new ECertViewModel
            {
                Status = AppResources.RequestingStatus
            };
            BindingContext = vm;

            UpdateView();
        }


        public CertDetailsPage(ECert cert, bool showExec = false)
        {

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            vm = new ECertViewModel(cert)
            {
                Status = AppResources.RequestingStatus
            };
            BindingContext = vm;

            ExecArea.IsVisible = showExec;

            UpdateView();
        }





        private List<KeyValuePair<string, string>> certFields = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("HEADER", AppResources.CertificateDetails),
            new KeyValuePair<string, string>("HEADER", AppResources.Status),
            new KeyValuePair<string, string>("Status", AppResources.Status),
            new KeyValuePair<string, string>("IssueDateDep", AppResources.CertIssueDate),
            new KeyValuePair<string, string>("ExpDateDep", AppResources.CertExpDate),
            //new KeyValuePair<string, string>("TransactionId", AppResources.TransactionId),
            new KeyValuePair<string, string>("HEADER", AppResources.ExporterInfo),
            new KeyValuePair<string, string>("ExporterId", AppResources.ExporterId),
            new KeyValuePair<string, string>("HEADER", AppResources.ProducerInfo),
            new KeyValuePair<string, string>("ApplicationId", AppResources.ApplicationNumber),
            new KeyValuePair<string, string>("Facility.CompanyName", AppResources.CompanyName),
            new KeyValuePair<string, string>("Facility.Address1", AppResources.Address1),
            new KeyValuePair<string, string>("Facility.Address2", AppResources.Address2),
            new KeyValuePair<string, string>("Facility.City", AppResources.City),
            new KeyValuePair<string, string>("Facility.State", AppResources.State),
            new KeyValuePair<string, string>("Facility.Country", AppResources.Country),
            new KeyValuePair<string, string>("Facility.ZipCode", AppResources.PostalCode),
            new KeyValuePair<string, string>("Facility.ContactFirstName", AppResources.ContactFirstname),
            new KeyValuePair<string, string>("Facility.ContactLastName", AppResources.ContactLastname),
            new KeyValuePair<string, string>("Facility.ContactEmail", AppResources.ContactEmail),
            new KeyValuePair<string, string>("Facility.ContactTelephone", AppResources.ContactPhone),
            new KeyValuePair<string, string>("HEADER", AppResources.ShipmentInfo),
            new KeyValuePair<string, string>("Shipment.ContainerNumber", AppResources.ContainerNumber),
            new KeyValuePair<string, string>("Shipment.PlaceOfLoading", AppResources.PointOfDeparture),
            new KeyValuePair<string, string>("Shipment.PointOfEntry", AppResources.PointOfEntry),
            new KeyValuePair<string, string>("Shipment.ConditionsForTransport", AppResources.ConditionsForTransport),
            new KeyValuePair<string, string>("Shipment.ImporterName", AppResources.ImporterName),
            new KeyValuePair<string, string>("Shipment.Address1", AppResources.Address1),
            new KeyValuePair<string, string>("Shipment.Address2", AppResources.Address2),
            new KeyValuePair<string, string>("Shipment.City", AppResources.City),
            new KeyValuePair<string, string>("Shipment.State", AppResources.State),
            new KeyValuePair<string, string>("Shipment.Country", AppResources.Country),
            new KeyValuePair<string, string>("Shipment.ZipCode", AppResources.PostalCode),
            new KeyValuePair<string, string>("HEADER", AppResources.ProductInfo),
            new KeyValuePair<string, string>("Product.ProductName", AppResources.ProductName),
            new KeyValuePair<string, string>("Product.ProductDescription", AppResources.ProductDescription),
            new KeyValuePair<string, string>("Product.ProductManufacturedDate", AppResources.ManufacturedDate),
            new KeyValuePair<string, string>("Product.ProductExpirationDate", AppResources.ExpirationDate),
            new KeyValuePair<string, string>("Product.ProductQuantity", AppResources.Quantity)

        };

        private void UpdateView()
        {

            ViewArea.Children.Clear();

            ViewArea.ColumnDefinitions.Clear();
            ViewArea.RowDefinitions.Clear();

            ViewArea.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            ViewArea.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });

            Label lbl;
            int row = 0;

            foreach(var field in certFields)
            {
                if (field.Key == "HEADER")
                {
                    ViewArea.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

                    lbl = new Label()
                    {
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        VerticalTextAlignment = TextAlignment.Center,
                        FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                        TextColor = Color.Black,
                        Text = field.Value,
                        Margin = new Thickness(20, 20, 10, 5),
                        TextDecorations = TextDecorations.Underline
                    };

                    ViewArea.Children.Add(lbl);

                    lbl.SetValue(Grid.ColumnProperty, 0);
                    lbl.SetValue(Grid.RowProperty, row);


                }
                else
                {

                    ViewArea.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

                    lbl = new Label()
                    {
                        HorizontalOptions = LayoutOptions.Fill,
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalTextAlignment = TextAlignment.End,
                        VerticalTextAlignment = TextAlignment.Center,
                        FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                        TextColor = Color.Black,
                        Margin = new Thickness(40, 5, 20, 5),
                        Text = field.Value + ": "
                    };

                    ViewArea.Children.Add(lbl);

                    lbl.SetValue(Grid.ColumnProperty, 0);
                    lbl.SetValue(Grid.RowProperty, row);


                    lbl = new Label()
                    {
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        VerticalTextAlignment = TextAlignment.Center,
                        FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                        TextColor = Color.Black,
                        Margin = new Thickness(10, 5),
                        FontAttributes = FontAttributes.Bold
                    };

                    ViewArea.Children.Add(lbl);

                    lbl.SetValue(Grid.ColumnProperty, 1);
                    lbl.SetValue(Grid.RowProperty, row);

                    lbl.SetBinding(Label.TextProperty, new Binding(field.Key));

                }

                row++;

            }

            new Task(async () =>
            {

                var status = await NetHelper.GetApplicationStatus(vm.Source);
                Device.BeginInvokeOnMainThread(() => vm.Status = status);

            }).Start();
        }

        private void BackBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void ApproveBtn_Clicked(object sender, EventArgs e)
        {

            vm.CertExpDate = DateTime.Now.AddDays(365);
            popup.IsOpen = true;

            // Navigation.PopAsync();
        }

        private async void RejectBtn_Clicked(object sender, EventArgs e)
        {
            await NetHelper.ProcessApplication(vm.Source, true);

            await Navigation.PopAsync();
        }

        private void CancelBtn_Clicked(object sender, EventArgs e)
        {
            popup.IsOpen = false;
        }

        private async void OKBtn_Clicked(object sender, EventArgs e)
        {
            popup.IsOpen = false;

            await NetHelper.ProcessApplication(vm.Source, false);
            await Navigation.PopAsync();
        }
    }
}