﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using BloqCert.Helpers;
using BloqCert.ViewModels;
using BloqCert.Models;

namespace BloqCert.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CertListPage : ContentPage
    {



        private List<ECertViewModel> certList;

        public bool IsExporter { get; private set; } = false;

        public CertListPage()
        {

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

        }

        public CertListPage(bool exporter)
        {

            IsExporter = exporter;

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();


        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await RefreshApplications();            

        }

        public async Task RefreshApplications()
        {

            List<ECert> l;

            if (IsExporter)
            {
                l = await NetHelper.GetApplications(Settings.ExporterId);

                certList = new List<ECertViewModel>();

                if (l == null) l = new List<ECert>();

                foreach (var item in l)
                {
                    certList.Add(new ECertViewModel(item));
                }

                Certs.ItemsSource = certList;

            }
            else
            {

                l = await NetHelper.GetApplications();
                if (l == null) l = new List<ECert>();

                try
                {
                    l.AddRange(await NetHelper.GetApplications(CertListType.Accepted));
                    l.AddRange(await NetHelper.GetApplications(CertListType.Rejected));
                }
                catch
                {

                }

                if (l == null) l = new List<ECert>();

                certList = new List<ECertViewModel>();

                foreach (var item in l)
                {
                    certList.Add(new ECertViewModel(item));
                }

                Certs.ItemsSource = certList;
            }
        }

        private void BackBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private async void Certs_SelectionChanged(object sender, Telerik.XamarinForms.DataGrid.DataGridSelectionChangedEventArgs e)
        {

            int x = 0;

            foreach (var item in e.AddedItems)
            {

                if (item is ECertViewModel c)
                {
                    if (c.Status == "Requested" && !IsExporter)
                        await Navigation.PushAsync(new CertDetailsPage(c.Source, true));
                    else
                        await Navigation.PushAsync(new CertDetailsPage(c.Source, false));

                }
                if (++x >= 1) break;
            }

        }
    }
}