﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace BloqCert.UserControls 
{
    public class CustomDatePicker : DatePicker
    {

        public static readonly BindableProperty CornerRadiusProperty
        = BindableProperty.Create(propertyName: "CornerRadius",
                                  returnType: typeof(double),
                                  declaringType: typeof(CustomDatePicker),
                                  defaultValue: 0D);

        public double CornerRadius
        {
            get
            {
                return (double)GetValue(CornerRadiusProperty);
            }
            set
            {
                SetValue(CornerRadiusProperty, value);
            }

        }

        public static readonly BindableProperty BorderWidthProperty
          = BindableProperty.Create(propertyName: "BorderWidth",
                                    returnType: typeof(double),
                                    declaringType: typeof(CustomDatePicker),
                                    defaultValue: 1D);

        public double BorderWidth
        {
            get
            {
                return (double)GetValue(BorderWidthProperty);
            }
            set
            {
                SetValue(BorderWidthProperty, value);
            }

        }


        public static readonly BindableProperty PaddingProperty
            = BindableProperty.Create(propertyName: "Padding",
                                      returnType: typeof(Thickness),
                                      declaringType: typeof(CustomDatePicker),
                                      defaultValue: new Thickness(0));

        public Thickness Padding
        {
            get
            {
                return (Thickness)GetValue(PaddingProperty);
            }
            set
            {
                SetValue(PaddingProperty, value);
            }

        }


        public static readonly BindableProperty BorderColorProperty
            = BindableProperty.Create(propertyName: "BorderColor",
                                      returnType: typeof(Color),
                                      declaringType: typeof(CustomDatePicker),
                                      defaultValue: Color.Black);

        public Color BorderColor
        {
            get
            {
                return (Color)GetValue(BorderColorProperty);
            }
            set
            {
                SetValue(BorderColorProperty, value);
            }

        }


        public static readonly new BindableProperty TextColorProperty
            = BindableProperty.Create(propertyName: "TextColor",
                                      returnType: typeof(Color),
                                      declaringType: typeof(CustomDatePicker),
                                      defaultValue: Color.Black);

        public new Color TextColor
        {
            get
            {
                return (Color)GetValue(TextColorProperty);
            }
            set
            {
                SetValue(TextColorProperty, value);
            }

        }

        public static readonly BindableProperty WatermarkTextColorProperty
            = BindableProperty.Create(propertyName: "WatermarkTextColor",
                                      returnType: typeof(Color),
                                      declaringType: typeof(CustomDatePicker),
                                      defaultValue: Color.Black);

        public Color WatermarkTextColor
        {
            get
            {
                return (Color)GetValue(WatermarkTextColorProperty);
            }
            set
            {
                SetValue(WatermarkTextColorProperty, value);
            }

        }


        public static readonly BindableProperty WatermarkTextProperty
            = BindableProperty.Create(propertyName: "WatermarkText", 
                                      returnType: typeof(string), 
                                      declaringType: typeof(CustomDatePicker), 
                                      defaultValue: default(string));

        public string WatermarkText
        {
            get
            {
                return (string)GetValue(WatermarkTextProperty);
            }
            set
            {
                SetValue(WatermarkTextProperty, value);
            }

        }

    }
}
