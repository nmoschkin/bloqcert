﻿using BloqCert.Helpers;
using BloqCert.Localization.Resources;
using BloqCert.Views.LoginScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BloqCert.Views.MainScreen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FDAView : ContentView
    {
        public FDAView()
        {
            InitializeComponent();



            WelcomeText.Text = string.Format(AppResources.WelcomeX, ((App)App.Current).User.Username);
            IDText.Text = "";

        }

        private async void CertRec_Clicked(object sender, EventArgs e)
        {

            await Navigation.PushAsync(new CertListPage());

        }

        private void Logout_Clicked(object sender, EventArgs e)
        {

            NetHelper.LoginToken = null;
            var ls = new LoginScreen.LoginScreen();

            ls.LoadView(ViewType.LoginView);
            App.Current.MainPage = ls;
        }

    }
}