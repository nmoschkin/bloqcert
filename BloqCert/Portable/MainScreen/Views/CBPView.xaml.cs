﻿using BloqCert.Helpers;
using BloqCert.Localization.Resources;
using BloqCert.Views.LoginScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BloqCert.Helpers;
using BloqCert.Interfaces;

namespace BloqCert.Views.MainScreen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CBPView : ContentView
    {
        public CBPView()
        {
            InitializeComponent();

            WelcomeText.Text = string.Format(AppResources.WelcomeX, ((App)App.Current).User.Username);
            IDText.Text = "";

        }

        private void Logout_Clicked(object sender, EventArgs e)
        {

            NetHelper.LoginToken = null;
            var ls = new LoginScreen.LoginScreen();

            ls.LoadView(ViewType.LoginView);
            App.Current.MainPage = ls;
        }

        private void ViewCert_Clicked(object sender, EventArgs e)
        {
            popup.IsOpen = true;
            // CertificateId.Focus();
        }

        private void CancelBtn_Clicked(object sender, EventArgs e)
        {
            //CertificateId.Unfocus();
            //DependencyService.Get<IKeyboardDismissService>().DismissKeyboard();
            popup.IsOpen = false;
        }

        private async void OKBtn_Clicked(object sender, EventArgs e)
        {

            //CertificateId.Unfocus();
            //DependencyService.Get<IKeyboardDismissService>().DismissKeyboard();
            popup.IsOpen = false;

            var cert = await NetHelper.GetApplicationById(CertificateId.Text);

            if (cert != null)
            {
                await Navigation.PushAsync(new CertDetailsPage(cert));
            }
        }
    }
}