﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BloqCert.Localization.Resources;
using BloqCert.Helpers;
using BloqCert.Views.LoginScreen;

namespace BloqCert.Views.MainScreen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImporterView : ContentView
    {
        public ImporterView()
        {
            InitializeComponent();

            WelcomeText.Text = string.Format(AppResources.WelcomeX, ((App)App.Current).User.Username);
            IDText.Text = "";

        }


        private void Logout_Clicked(object sender, EventArgs e)
        {

            NetHelper.LoginToken = null;
            var ls = new LoginScreen.LoginScreen();

            ls.LoadView(ViewType.LoginView);
            App.Current.MainPage = ls;
        }

    }
}