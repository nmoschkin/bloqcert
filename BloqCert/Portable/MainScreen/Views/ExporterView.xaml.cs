﻿using BloqCert.Localization.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BloqCert.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BloqCert.Views.LoginScreen;
using BloqCert.Helpers;

namespace BloqCert.Views.MainScreen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExporterView : ContentView
    {
        public ExporterView()
        {
            InitializeComponent();


            WelcomeText.Text = string.Format(AppResources.WelcomeX, ((App)App.Current).User.Username);
            IDText.Text = "";
        }

        private void StartApp_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Views.AppScreen.AppScreen());
        }

        private void TrackStatus_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Views.CertDetailsPage());

        }

        private void ViewCert_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Views.CertListPage(true));
        }

        private void Logout_Clicked(object sender, EventArgs e)
        {

            NetHelper.LoginToken = null;
            var ls = new LoginScreen.LoginScreen();

            ls.LoadView(ViewType.LoginView);
            App.Current.MainPage = ls;
        }

    }
}