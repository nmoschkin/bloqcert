using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using BloqCert.ViewModels;
using BloqCert.Models;

namespace BloqCert.Views.MainScreen
{
  
    public partial class MainScreen : ContentPage
    {

        private MainPageViewModel vm;

        public MainScreen()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            ConfigVM();
        }

        public MainScreen(CertRole role)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            ConfigVM(role);
        }

        private void ConfigVM(CertRole role = CertRole.FDA)
        {

            if (vm != null)
            {
                vm.PropertyChanged -= Vm_PropertyChanged;
            }

            vm = new MainPageViewModel(role);
            BindingContext = vm;

            ConfigView();

            vm.PropertyChanged += Vm_PropertyChanged;

        }

        private void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Role")
            {
                ConfigView();
            }
        }

        private void ConfigView()
        {

            ViewArea.Children.Clear();

            switch (vm.Role)
            {

                case CertRole.FDA:

                    ViewArea.Children.Add(new FDAView() { BindingContext = vm });
                    break;

                case CertRole.CBP:

                    ViewArea.Children.Add(new CBPView() { BindingContext = vm });
                    break;

                case CertRole.Exporter:

                    ViewArea.Children.Add(new ExporterView() { BindingContext = vm });
                    break;

                case CertRole.Importer:

                    //ViewArea.Children.Add(new ImporterView() { BindingContext = vm });
                    ViewArea.Children.Add(new CBPView() { BindingContext = vm });
                    break;

                case CertRole.Admin:

                    ViewArea.Children.Add(new AdminView() { BindingContext = vm });
                    break;
            }
        }

        public CertRole Role
        {
            get => vm != null ? vm.Role : CertRole.FDA;
        }


        private void FDABtn_Clicked(object sender, EventArgs e)
        {
            Drawer.IsOpen = false;
            Settings.Role = CertRole.FDA;
            ConfigVM(Settings.Role);
        }

        private void CBPBtn_Clicked(object sender, EventArgs e)
        {
            Drawer.IsOpen = false;
            Settings.Role = CertRole.CBP;
            ConfigVM(Settings.Role);
        }

        private void ExporterBtn_Clicked(object sender, EventArgs e)
        {
            Drawer.IsOpen = false;
            Settings.Role = CertRole.Exporter;
            ConfigVM(Settings.Role);

        }

        private void ImporterBtn_Clicked(object sender, EventArgs e)
        {
            Drawer.IsOpen = false;
            Settings.Role = CertRole.Importer;
            ConfigVM(Settings.Role);
        }

        private void AdminBtn_Clicked(object sender, EventArgs e)
        {
            Drawer.IsOpen = false;
            Settings.Role = CertRole.Admin;
            ConfigVM(Settings.Role);
        }
    }
}
