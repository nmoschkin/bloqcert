﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BloqCert
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Styles : ResourceDictionary
    {
		public Styles()
		{
			InitializeComponent();
		}
	}
}