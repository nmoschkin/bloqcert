﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloqCert.Interfaces
{
    public interface IKeyboardDismissService
    {

        void DismissKeyboard();

    }
}
