﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BloqCert.Localization.Resources;
using BloqCert.Models;
using Newtonsoft.Json;
using Xamarin.Essentials;

namespace BloqCert.Helpers
{

    public enum CertListType
    {
        Pending,
        Accepted,
        Rejected
    }



    public static class NetHelper
    {

        public const string APPLICATION_URL = "http://52.206.133.131:4000/api/applyCertificate";
        public const string STATUS_URL = "http://52.206.133.131:4000/api/query/getCertificateStatus";

        public const string PENDING_URL = "http://52.206.133.131:4000/api/getPendingCertificate";
        public const string ACCEPTED_URL = "http://52.206.133.131:4000/api/getAcceptedCertificate";
        public const string REJECTED_URL = "http://52.206.133.131:4000/api/getRejectedCertificate";

        public const string ACCEPT_APP_URL = "http://52.206.133.131:4000/api/acceptCertificate";
        public const string REJECT_APP_URL = "http://52.206.133.131:4000/api/rejectCertificate";


        public const string EXPORTER_CERT_URL = "http://52.206.133.131:4000/api/query/getCertificateByExporterId";
        public const string LOGIN_URL = "http://52.206.133.131:4000/login";
        public const string REGISTER_URL = "http://52.206.133.131:4000/register";


        public const string GET_CERT_URL = "http://52.206.133.131:4000/api/query/getCertificateById";
   
            
        private static HttpClient client;

        public static bool IsBusy { get; private set; } = false;


        public static string LoginToken { get; set; }


        static NetHelper()
        {

            client = new HttpClient();

        }

        public static async Task<bool> CheckNetwork()
        {

            if ((Connectivity.NetworkAccess & NetworkAccess.Internet) == NetworkAccess.None)
            {
                await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, AppResources.ErrorNoInternet, AppResources.OK);
                return false;
            }

            return true;

        }


        public static HttpRequestMessage ConstructRequest(string url, HttpMethod method, bool useToken = true)
        {

            var request = new HttpRequestMessage();

            request.RequestUri = new Uri(url);
            request.Method = method;

            if (useToken && !string.IsNullOrEmpty(LoginToken))
            {
                request.Headers.Add("Authorization", "Bearer " + LoginToken);
            }

            return request;

        }


        public static async Task<LoginResult> Login(string username, string password) => await Login(new LoginModel(username, password));

        public static async Task<LoginResult> Login(LoginModel login)
        {

            var request = ConstructRequest(LOGIN_URL, HttpMethod.Post, false);
            var logstr = JsonConvert.SerializeObject(login);

            request.Content = new StringContent(logstr, Encoding.UTF8, "application/json");

            if (IsBusy) return null;

            if (!await CheckNetwork()) return null;

            IsBusy = true;

            HttpResponseMessage response;

            LoginResult resp = null;

            try
            {

                response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();

                    if (json != null)
                    {
                        resp = JsonConvert.DeserializeObject<LoginResult>(json);
                        LoginToken = resp.Token;
                    }

                }
                else
                {
                    await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, $"{AppResources.ErrorCannotConnect}\r\n{response.StatusCode.ToString()}", AppResources.OK);
                }

                response.Dispose();
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex.Message, AppResources.OK);
            }
            finally
            {
                IsBusy = false;
                request.Dispose();
            }

            return resp;
        }

        public static async Task<bool> CreateUser(string username, string password, string role)
        {

            var request = ConstructRequest(REGISTER_URL, HttpMethod.Post);
            var logstr = JsonConvert.SerializeObject(new UserInfo() { Username = username, Password = password, Role = role });

            request.Content = new StringContent(logstr, Encoding.UTF8, "application/json");

            if (IsBusy) return false;

            if (!await CheckNetwork()) return false;

            IsBusy = true;

            HttpResponseMessage response;

            RegUserResult resp = null;

            try
            {

                response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();

                    if (json != null)
                    {
                        resp = JsonConvert.DeserializeObject<RegUserResult>(json);
                    }

                }
                else
                {
                    await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, $"{AppResources.ErrorCannotConnect}\r\n{response.StatusCode.ToString()}", AppResources.OK);
                }

                response.Dispose();
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex.Message, AppResources.OK);
            }
            finally
            {
                request.Dispose();
            }

            IsBusy = false;
            return (bool)resp?.Success;
        }



        public static async Task<List<ECert>> GetApplications(CertListType type = CertListType.Pending)
        {
            if (IsBusy) return null;

            if (!await CheckNetwork()) return null;

            IsBusy = true;

            List<ECert> recOut = new List<ECert>();
            CertArrayResult recIn;

            HttpResponseMessage response;
            HttpRequestMessage request = null;

            try
            {

                switch (type)
                {
                    case CertListType.Pending:

                        request = ConstructRequest(PENDING_URL, HttpMethod.Get);
                        break;

                    case CertListType.Accepted:

                        request = ConstructRequest(ACCEPTED_URL, HttpMethod.Get);
                        break;

                    case CertListType.Rejected:

                        request = ConstructRequest(REJECTED_URL, HttpMethod.Get);
                        break;

                }

                response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();

                    if (json != null)
                    {

                        try
                        {

                            var settings = new JsonSerializerSettings()
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            };

                            recIn = JsonConvert.DeserializeObject<CertArrayResult>(json, settings);

                            foreach (var rec in recIn.Data)
                            {
                                recOut.Add(rec.Record);
                            }

                        }
                        catch(Exception ex2)
                        {
                            await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex2.Message, AppResources.OK);
                        }

                    }

                }
                else
                {
                    await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, $"{AppResources.ErrorCannotConnect}\r\n{response.StatusCode.ToString()}", AppResources.OK);
                }

                response.Dispose();
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex.Message, AppResources.OK);
            }
            finally
            {
                request.Dispose();
            }

            IsBusy = false;
            return recOut;
        }


        public static async Task<List<ECert>> GetApplications(string exporterId)
        {
            if (IsBusy) return null;

            if (!await CheckNetwork()) return null;

            IsBusy = true;

            List<ECert> recOut = new List<ECert>();
            List<CertKeyRecord> recIn = null;

            HttpResponseMessage response;
            HttpRequestMessage request = null;

            try
            {

                request = ConstructRequest(EXPORTER_CERT_URL + "?exporterId=" + exporterId, HttpMethod.Get);
                response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();

                    if (json != null)
                    {

                        try
                        {

                            var settings = new JsonSerializerSettings()
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            };

                            var resIn = JsonConvert.DeserializeObject<CertArrayResult>(json, settings);

                            foreach (var rec in resIn.Data)
                            {
                                recOut.Add(rec.Record);
                            }

                        }
                        catch (Exception ex2)
                        {
                            await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex2.Message, AppResources.OK);
                        }

                    }

                }
                else
                {
                    await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, $"{AppResources.ErrorCannotConnect}\r\n{response.StatusCode.ToString()}", AppResources.OK);
                }

                response.Dispose();
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex.Message, AppResources.OK);
            }
            finally
            {
                IsBusy = false;
                request?.Dispose();
            }

            IsBusy = false;
            return recOut;
        }


        public static async Task<ECert> GetApplicationById(string applicationId)
        {
            if (IsBusy) return null;

            if (!await CheckNetwork()) return null;

            IsBusy = true;

            ECert ret= null;
            HttpResponseMessage response;
            HttpRequestMessage request = null;

            try
            {

                request = ConstructRequest(GET_CERT_URL + "?applicationId=" + applicationId, HttpMethod.Get);
                response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();

                    if (json != null)
                    {

                        try
                        {

                            var settings = new JsonSerializerSettings()
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            };

                            var resIn = JsonConvert.DeserializeObject<CertArrayResult>(json, settings);
                            ret = resIn.Data[0].Record;
                            

                        }
                        catch (Exception ex2)
                        {
                            await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex2.Message, AppResources.OK);
                        }

                    }

                }
                else
                {
                    await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, $"{AppResources.ErrorCannotConnect}\r\n{response.StatusCode.ToString()}", AppResources.OK);
                }

                response.Dispose();
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex.Message, AppResources.OK);
            }
            finally
            {
                IsBusy = false;
                request?.Dispose();
            }

            IsBusy = false;
            return ret;
        }


        public static async Task<string> GetApplicationStatus(ECert cert)
        {
            if (IsBusy) return null;

            if (!await CheckNetwork()) return null;

            IsBusy = true;

            HttpResponseMessage response;
            HttpRequestMessage request = null;

            ServerResponse resp = null;

            try
            {

                request = ConstructRequest(STATUS_URL + $"?applicationId={cert.ApplicationId}", HttpMethod.Get);
                
                response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();

                    if (json != null)
                    {
                        //var ser = new JsonSerializerSettings()
                        //{
                            
                        //};
                        resp = JsonConvert.DeserializeObject<ServerResponse>(json);
                    }

                }
                else
                {
                    await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, $"{AppResources.ErrorCannotConnect}\r\n{response.StatusCode.ToString()}", AppResources.OK);
                }

                response.Dispose();
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex.Message, AppResources.OK);
            }
            finally
            {
                request?.Dispose();
            }

            IsBusy = false;
            return resp?.Status ?? "Invalid";
        }

        public static async Task<ServerResponse> SendApplication(ECert cert)
        {

            if (IsBusy) return null;

            if (!await CheckNetwork()) return null;

            IsBusy = true;

            HttpResponseMessage response;
            HttpRequestMessage request = null;

            ServerResponse resp = null;

            try
            {
                cert.Status = "Requested";

                cert.CertExpDate = DateTime.MinValue;
                cert.CertIssueDate = DateTime.MinValue;

                //cert.Product.ProductExpirationDate = DateTime.Now.AddMonths(24);
                //cert.Product.ProductManufacturedDate = DateTime.Now;

                string jsonOut = JsonConvert.SerializeObject(cert);
                jsonOut = jsonOut.Replace("null", "\"\"");

                request = ConstructRequest(APPLICATION_URL, HttpMethod.Post);
                request.Content = new StringContent(jsonOut, Encoding.UTF8, "application/json");
                
                response = await client.SendAsync(request);
                
                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();

                    if (json != null)
                    {
                        resp = JsonConvert.DeserializeObject<ServerResponse>(json);
                    }

                }
                else
                {
                    await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, $"{AppResources.ErrorCannotConnect}\r\n{response.StatusCode.ToString()}", AppResources.OK);
                }

                response.Dispose();
            }
            catch(Exception ex)
            {
                await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex.Message, AppResources.OK);
            }
            finally
            {
                request?.Dispose();
            }


            IsBusy = false;
            return resp;

        }

        public static async Task<bool> ProcessApplication(ECert cert, bool reject = false)
        {
            if (IsBusy) return false;

            if (!await CheckNetwork()) return false;

            IsBusy = true;

            HttpResponseMessage response;
            HttpRequestMessage request = null;

            APIResult resp = null;

            try
            {
                request = ConstructRequest((reject ? REJECT_APP_URL : ACCEPT_APP_URL), HttpMethod.Post);

                string body;

                if (reject)
                {
                    var rej = new RejectCert() { ApplicationId = cert.ApplicationId, Comment = "" };
                    body = JsonConvert.SerializeObject(rej);
                }
                else
                {
                    var acc = new AcceptCert() { ApplicationId = cert.ApplicationId, ExpirationDate = cert.CertExpDate };
                    body = JsonConvert.SerializeObject(acc);

                }

                request.Content = new StringContent(body, Encoding.UTF8, "application/json");

                response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();

                    if (json != null)
                    {
                        //var ser = new JsonSerializerSettings()
                        //{

                        //};
                        resp = JsonConvert.DeserializeObject<APIResult>(json);
                    }

                }
                else
                {
                    await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, $"{AppResources.ErrorCannotConnect}\r\n{response.StatusCode.ToString()}", AppResources.OK);
                }

                response.Dispose();
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert(AppResources.ErrorFailedCertificateRequest, ex.Message, AppResources.OK);
            }
            finally
            {
                request?.Dispose();
            }

            IsBusy = false;
            return (bool)resp?.Success;
        }



    }
}
