﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BloqCert.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Collections.ObjectModel;
using BloqCert.ViewModels;

namespace BloqCert.Helpers
{
    public class DatabaseHelper
    {
        public const string dbFileName = "BloqCert.db";

        public SQLiteAsyncConnection Conn { get; protected set; }

        public DatabaseHelper()
        {
            try
            {
                var path = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)), dbFileName);
                Conn = new SQLiteAsyncConnection(path);

                Conn.CreateTableAsync<ECertTable>();
            }
            catch // supressing errors for now.
            {
            }

        }


        public async Task<bool> SaveECertAsync(ECert cert)
        {
            try
            {
                await Conn.CreateTableAsync<ECertTable>();

                var save = ECertTable.FromECert(cert);

                await Conn.InsertOrReplaceAsync(save);
                Settings.LastSaveCert = save.ApplicationId;

                return true;
            }
            catch
            {
                return false;
            }

        }


        public async Task<ECert> LoadECertAsync(string appId)
        {
            try
            {
                var l = await Conn.QueryAsync<ECertTable>($"SELECT * FROM ECertTable WHERE ApplicationId='{appId}'");
                return l[0].Deserialize();
            }
            catch
            {
                return null;
            }
        }

        public bool SaveECert(ECert cert)
        {
            try
            {
                Conn.CreateTableAsync<ECertTable>();

                var save = ECertTable.FromECert(cert);

                Conn.InsertOrReplaceAsync(save);
                Settings.LastSaveCert = save.ApplicationId;

                return true;
            }
            catch
            {
                return false;
            }

        }


        public ECert LoadECert(string appId)
        {
            try
            {
                var l = Conn.QueryAsync<ECertTable>($"SELECT * FROM ECertTable WHERE ApplicationId='{appId}'").Result;
                return l[0].Deserialize();
            }
            catch
            {
                return null;
            }
        }

    }

}