﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using BloqCert.Views;

namespace BloqCert.ViewModels
{

    public enum CertRole
    {
        FDA,
        CBP,
        Exporter,
        Importer,
        Admin
    }

    public class MainPageViewModel : INotifyPropertyChanged
    {

        private CertRole role = CertRole.FDA;
        private WeakReference<BloqCert.Views.MainScreen.MainScreen> view;

        public CertRole Role
        {
            get => role;
            set
            {
                if (role == value) return;

                role = value;
                OnPropertyChanged();
            }
        }


        public MainPageViewModel()
        {

        }

        public MainPageViewModel(CertRole role)
        {
            Role = role;
        }


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName()] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
