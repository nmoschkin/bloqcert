﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using BloqCert.Models;
using System.Runtime.CompilerServices;
using BloqCert.Helpers;

namespace BloqCert.ViewModels
{
    public class ProductViewModel : INotifyPropertyChanged
    {

        public Product Source { get; }

        public ProductViewModel(Product source)
        {
            Source = source;
        }

        public ProductViewModel()
        {
            Source = new Product();
        }

        public string ProductName
        {
            get => Source.ProductName;
            set
            {
                if (Source.ProductName == value) return;
                Source.ProductName = value;
                OnPropertyChanged();
            }
        }


        public string ProductDescription
        {
            get => Source.ProductDescription;
            set
            {
                if (Source.ProductDescription == value) return;
                Source.ProductDescription = value;
                OnPropertyChanged();
            }
        }


        public DateTime ProductManufacturedDate
        {
            get => Source.ProductManufacturedDate;
            set
            {
                if (Source.ProductManufacturedDate == value) return;
                Source.ProductManufacturedDate = value;
                OnPropertyChanged();
            }
        }


        public DateTime ProductExpirationDate
        {
            get => Source.ProductExpirationDate;
            set
            {
                if (Source.ProductExpirationDate == value) return;
                Source.ProductExpirationDate = value;
                OnPropertyChanged();
            }
        }


        public string ProductQuantity
        {
            get => Source.ProductQuantity;
            set
            {
                if (Source.ProductQuantity == value) return;
                Source.ProductQuantity = value;
                OnPropertyChanged();
            }
        }



        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName()] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }

}
