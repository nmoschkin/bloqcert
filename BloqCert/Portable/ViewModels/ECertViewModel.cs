﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using BloqCert.Models;
using System.Runtime.CompilerServices;
using BloqCert.Helpers;
using BloqCert.Localization.Resources;

namespace BloqCert.ViewModels
{


    public class ECertViewModel : INotifyPropertyChanged
    {

        public ECert Source { get; }

        public ECertViewModel(ECert source)
        {
            Source = source;

            Shipment = new ShipmentViewModel(Source.Shipment);
            Facility = new FacilityViewModel(Source.Facility);
            Product = new ProductViewModel(Source.Product);
        }

        public ECertViewModel()
        {

            if (!string.IsNullOrEmpty(Settings.LastSaveCert))
            {
                var db = new DatabaseHelper();
                Source = db.LoadECert(Settings.LastSaveCert);

            }
            else
            {
                Source = new ECert(true);
            }

            Shipment = new ShipmentViewModel(Source.Shipment);
            Facility = new FacilityViewModel(Source.Facility);
            Product = new ProductViewModel(Source.Product);
        }

        public void AssignNewId()
        {
            Source.AssignNewId();
            OnPropertyChanged("ApplicationId");
        }

        public string TransactionId
        {
            get => Source.TransactionId;
            set
            {
                if (Source.TransactionId == value) return;
                Source.TransactionId = value;
                OnPropertyChanged();
            }
        }


        public string ApplicationId
        {
            get => Source.ApplicationId;
            set
            {
                if (Source.ApplicationId == value) return;
                Source.ApplicationId = value;
                OnPropertyChanged();
            }
        }


        public string ExporterId
        {
            get => Source.ExporterId;
            set
            {
                if (Source.ExporterId == value) return;
                Source.ExporterId = value;
                OnPropertyChanged();
            }
        }



        //public string DestinationCountry
        //{
        //    get => Source.DestinationCountry;
        //    set
        //    {
        //        if (Source.DestinationCountry == value) return;
        //        Source.DestinationCountry = value;
        //        OnPropertyChanged();
        //    }
        //}


        public string Comment
        {
            get => Source.Comment;
            set
            {
                if (Source.Comment == value) return;
                Source.Comment = value;
                OnPropertyChanged();
            }
        }


        public string IssueDateDep
        {
            get
            {
                if (Source.Status != "Accepted") return AppResources.NotApplicable;
                else return CertIssueDate.ToString("d");
            }
        }

        public DateTime CertIssueDate
        {
            get => Source.CertIssueDate;
            set
            {
                if (Source.CertIssueDate == value) return;
                Source.CertIssueDate = value;
                OnPropertyChanged();
            }
        }


        public string ExpDateDep
        {
            get
            {
                if (Source.Status != "Accepted") return AppResources.NotApplicable;
                else return CertExpDate.ToString("d");
            }
        }

        public DateTime CertExpDate
        {
            get => Source.CertExpDate;
            set
            {
                if (Source.CertExpDate == value) return;
                Source.CertExpDate = value;
                OnPropertyChanged();
            }
        }

        public FacilityViewModel Facility { get; }

        public ProductViewModel Product { get; }

        public ShipmentViewModel Shipment { get; }

        public string Status
        {
            get => Source.Status;
            set
            {
                if (Source.Status == value) return;
                Source.Status = value;
                OnPropertyChanged();
                OnPropertyChanged("IssueDateDep");
                OnPropertyChanged("ExpDateDep");
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName()] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
