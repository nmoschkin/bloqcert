﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using BloqCert.Models;
using System.Runtime.CompilerServices;
using BloqCert.Helpers;

namespace BloqCert.ViewModels
{
    public class FacilityViewModel : INotifyPropertyChanged
    {
        public Facility Source { get; }

        public FacilityViewModel(Facility source)
        {
            Source = source;
        }

        public FacilityViewModel()
        {
            Source = new Facility();
        }

        public string RegId
        {
            get => Source.RegId;
            set
            {
                if (Source.RegId == value) return;
                Source.RegId = value;
                OnPropertyChanged();
            }
        }


        public string CompanyName
        {
            get => Source.CompanyName;
            set
            {
                if (Source.CompanyName == value) return;
                Source.CompanyName = value;
                OnPropertyChanged();
            }
        }


        public string Location
        {
            get => Source.Location;
            set
            {
                if (Source.Location == value) return;
                Source.Location = value;
                OnPropertyChanged();
            }
        }


        public string ContactFirstName
        {
            get => Source.ContactFirstName;
            set
            {
                if (Source.ContactFirstName == value) return;
                Source.ContactFirstName = value;
                OnPropertyChanged();
            }
        }


        public string ContactLastName
        {
            get => Source.ContactLastName;
            set
            {
                if (Source.ContactLastName == value) return;
                Source.ContactLastName = value;
                OnPropertyChanged();
            }
        }


        public string ContactEmail
        {
            get => Source.ContactEmail;
            set
            {
                if (Source.ContactEmail == value) return;
                Source.ContactEmail = value;
                OnPropertyChanged();
            }
        }

        public string ContactTelephone
        {
            get => Source.ContactTelephone;
            set
            {
                if (Source.ContactTelephone == value) return;
                Source.ContactTelephone = value;
                OnPropertyChanged();
            }
        }

        public string Address1
        {
            get => Source.Address1;
            set
            {
                if (Source.Address1 == value) return;
                Source.Address1 = value;
                OnPropertyChanged();
                OnPropertyChanged("FullAddress");
            }
        }
        public string Address2
        {
            get => Source.Address2;
            set
            {
                if (Source.Address2 == value) return;
                Source.Address2 = value;
                OnPropertyChanged();
                OnPropertyChanged("FullAddress");
            }
        }

        public string ZipCode
        {
            get => Source.ZipCode;
            set
            {
                if (Source.ZipCode == value) return;
                Source.ZipCode = value;
                OnPropertyChanged();
                OnPropertyChanged("FullAddress");
            }
        }

        public string City
        {
            get => Source.City;
            set
            {
                if (Source.City == value) return;
                Source.City = value;
                OnPropertyChanged();
                OnPropertyChanged("FullAddress");
            }
        }


        public string State
        {
            get => Source.State;
            set
            {
                if (Source.State == value) return;
                Source.State = value;
                OnPropertyChanged();
                OnPropertyChanged("FullAddress");
            }
        }


        public string Country
        {
            get => Source.Country;
            set
            {
                if (Source.Country == value) return;
                Source.Country = value;
                OnPropertyChanged();
                OnPropertyChanged("FullAddress");
            }
        }

        public string FullAddress
        {
            get
            {
                string s;
                string nl = "\r\n";

                s = Address1;
                if (!string.IsNullOrEmpty(Address2)) s += $"{nl}{Address2}";

                s += $"{nl}{City}, {State}";
                s += $"{nl}{Country}, {ZipCode}";

                return s;
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName()] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

}
