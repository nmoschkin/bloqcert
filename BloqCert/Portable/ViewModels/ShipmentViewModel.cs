﻿using BloqCert.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace BloqCert.ViewModels
{
    public class ShipmentViewModel : INotifyPropertyChanged
    {

        public Shipment Source { get; }

        public ShipmentViewModel(Shipment source)
        {
            Source = source;
        }

        public ShipmentViewModel()
        {
            Source = new Shipment();
        }

        public string ContainerNumber
        {
            get => Source.ContainerNumber;
            set
            {
                if (Source.ContainerNumber == value) return;
                Source.ContainerNumber = value;
                OnPropertyChanged();
            }
        }


        public string PlaceOfLoading
        {
            get => Source.PlaceOfLoading;
            set
            {
                if (Source.PlaceOfLoading == value) return;
                Source.PlaceOfLoading = value;
                OnPropertyChanged();
            }
        }


        public string PointOfEntry
        {
            get => Source.PointOfEntry;
            set
            {
                if (Source.PointOfEntry == value) return;
                Source.PointOfEntry = value;
                OnPropertyChanged();
            }
        }


        public string ConditionsForTransport
        {
            get => Source.ConditionsForTransport;
            set
            {
                if (Source.ConditionsForTransport == value) return;
                Source.ConditionsForTransport = value;
                OnPropertyChanged();
            }
        }


        public string ImporterName
        {
            get => Source.ImporterName;
            set
            {
                if (Source.ImporterName == value) return;
                Source.ImporterName = value;
                OnPropertyChanged();
            }
        }


        public string Address1
        {
            get => Source.Address1;
            set
            {
                if (Source.Address1 == value) return;
                Source.Address1 = value;
                OnPropertyChanged();
            }
        }

        public string Address2
        {
            get => Source.Address2;
            set
            {
                if (Source.Address2 == value) return;
                Source.Address2 = value;
                OnPropertyChanged();
            }
        }


        public string ZipCode
        {
            get => Source.ZipCode;
            set
            {
                if (Source.ZipCode == value) return;
                Source.ZipCode = value;
                OnPropertyChanged();
            }
        }


        public string City
        {
            get => Source.City;
            set
            {
                if (Source.City == value) return;
                Source.City = value;
                OnPropertyChanged();
            }
        }


        public string State
        {
            get => Source.State;
            set
            {
                if (Source.State == value) return;
                Source.State = value;
                OnPropertyChanged();
            }
        }


        public string Country
        {
            get => Source.Country;
            set
            {
                if (Source.Country == value) return;
                Source.Country = value;
                OnPropertyChanged();
            }
        }



        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName()] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }

}
