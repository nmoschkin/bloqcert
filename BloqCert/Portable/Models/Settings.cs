﻿using BloqCert.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Essentials;

namespace BloqCert.Models
{
    public static class Settings
    {

        // Language Settings
        private const string LanguageKey = "Language";
        private const string LanguageDefault = "en";

        private const string IsMerticKey = "IsMetric";
        private const bool IsMerticDefault = false;

        private const string IsLoggedInKey = "IsLoggedIn";
        private const string CertRoleKey = "CertRole";

        private const string LastSaveCertKey = "LastSaveCert";
        private const string ExporterIdKey = "ExporterId";


        public static string ExporterId
        {
            get => Preferences.Get(ExporterIdKey, null);
            set
            {
                Preferences.Remove(ExporterIdKey);
                Preferences.Set(ExporterIdKey, value);
            }
        }


        public static string LastSaveCert
        {
            get => Preferences.Get(LastSaveCertKey, null);
            set
            {
                Preferences.Remove(LastSaveCertKey);
                Preferences.Set(LastSaveCertKey, value);
            }
        }

        public static CertRole Role
        {
            get => (CertRole)Preferences.Get(CertRoleKey, (int)CertRole.FDA);
            set
            {
                Preferences.Remove(CertRoleKey);
                Preferences.Set(CertRoleKey, (int)value);
            }
        }


        public static bool IsLoggedIn
        {

            get => Preferences.Get(IsLoggedInKey, false);
            set
            {
                Preferences.Remove(IsLoggedInKey);
                Preferences.Set(IsLoggedInKey, value);
            }
        }


        public static string Language
        {

            get => Preferences.Get(LanguageKey, LanguageDefault);
            set
            {
                Preferences.Remove(LanguageKey);
                Preferences.Set(LanguageKey, value);
            }
        }

        public static CultureInfo UserCulture
        {
            get
            {
                try
                {
                    if (Language == null) return null;

                    var x = new CultureInfo(Language);
                    return x;
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                Language = value.TwoLetterISOLanguageName;
            }
        }

        public static bool IsMetric
        {
            get => Preferences.Get(IsMerticKey, IsMerticDefault);
            set => Preferences.Set(IsMerticKey, value);
        }


    }
}
