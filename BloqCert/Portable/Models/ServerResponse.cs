﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BloqCert.Models
{
    public class ServerResponse
    {

        private string transactionId;

        [JsonProperty("Status")]
        public string Status { get; set; }

        [JsonProperty("applicationId")]
        public string ApplicationId { get; set; }


        [JsonProperty("transactionId")]
        public string TransactionId
        {
            get => transactionId;
            set
            {
                transactionId = value.Replace("Transaction ID: ", "");
            }
        }



    }
}
