﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BloqCert.Models
{

    public class CertKeyResult
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("message")]
        public List<CertKeyRecord> Results { get; set; }


    }


    public class CertKeyRecord
    {
        [JsonProperty("key")]
        public string Key { get; set; }


        [JsonProperty("record")]
        public ECert Record { get; set; }

    }

}
