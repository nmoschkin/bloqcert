﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SQLite;


namespace BloqCert.Models
{
    public class Certificate
    {

        [PrimaryKey()]
        [Column("ID")]
        [JsonProperty("ID")]
        public string ID { get; set; }

        [Column("IssuedDate")]
        [JsonProperty("IssuedDate")]
        public string IssuedDate { get; set; }


        [Column("ExpirationDate")]
        [JsonProperty("ExpirationDate")]
        public string ExpirationDate { get; set; }


        [Column("ExporterID")]
        [JsonProperty("ExporterID")]
        public string ExporterID { get; set; }


        [Column("ExporterName")]
        [JsonProperty("ExporterName")]
        public string ExporterName { get; set; }


        [Column("Exporting_Country")]
        [JsonProperty("Exporting_Country")]
        public string ExportingCountry { get; set; }


        [Column("ImporterID")]
        [JsonProperty("ImporterID")]
        public string ImporterID { get; set; }


        [Column("ImporterName")]
        [JsonProperty("ImporterName")]
        public string ImporterName { get; set; }


        [Column("final_destination")]
        [JsonProperty("final_destination")]
        public Address FinalDestination { get; set; }


        [Column("productName")]
        [JsonProperty("productName")]
        public string ProductName { get; set; }

    }
}
