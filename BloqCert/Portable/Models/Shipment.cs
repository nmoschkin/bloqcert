﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SQLite;

namespace BloqCert.Models
{
    public class Shipment
    {
        // ShipmentInformation
        [Column("containerNumber")]
        [JsonProperty("containerNumber")]
        public string ContainerNumber { get; set; }

        [Column("placeOfLoading")]
        [JsonProperty("placeOfLoading")]
        public string PlaceOfLoading { get; set; }

        [Column("pointOfEntry")]
        [JsonProperty("pointOfEntry")]
        public string PointOfEntry { get; set; }

        [Column("conditionsForTransport")]
        [JsonProperty("conditionsForTransport")]
        public string ConditionsForTransport { get; set; }

        [Column("importerName")]
        [JsonProperty("importerName")]
        public string ImporterName { get; set; }

        [Column("importerPrimaryStreetAddress")]
        [JsonProperty("importerPrimaryStreetAddress")]
        public string Address1 { get; set; }

        [Column("importerSecondaryStreetAddress")]
        [JsonProperty("importerSecondaryStreetAddress")]
        public string Address2 { get; set; }

        [Column("importerZipCode")]
        [JsonProperty("importerZipCode")]
        public string ZipCode { get; set; }

        [Column("importerCity")]
        [JsonProperty("importerCity")]
        public string City { get; set; }

        [Column("importerState")]
        [JsonProperty("importerState")]
        public string State { get; set; }

        [Column("importerCountry")]
        [JsonProperty("importerCountry")]
        public string Country { get; set; }

    }
}
