﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloqCert.Models
{

    public class ECertTable
    {

        [PrimaryKey()]
        public string ApplicationId { get; set; }

        public string TransactionId { get; set; }

        public string JsonBlob { get; set; }


        public static ECertTable FromECert(ECert value)
        {
            return new ECertTable()
            {
                ApplicationId = value.ApplicationId,
                TransactionId = value.TransactionId,
                JsonBlob = JsonConvert.SerializeObject(value)
            };
        }

        public ECert Deserialize()
        {
            ECert output;

            try
            {
                output = JsonConvert.DeserializeObject<ECert>(JsonBlob);
                output.TransactionId = TransactionId;
            }
            catch
            {
                return null;
            }


            return output;
        }


    }
}
