﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BloqCert.Models
{
    public class UserInfo
    {

        [JsonIgnore()]
        public string UserId { get; set; }

        [JsonIgnore()]
        public string Firstname { get; set; }

        [JsonIgnore()]
        public string Lastname { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonIgnore()]
        public string Email { get; set; }

        [JsonIgnore()]
        public string OrganizationID { get; set; }

        public string[] Permissions { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("attrs")]
        public string[] Attributes { get; set; } = new string[] { };

    }
}
