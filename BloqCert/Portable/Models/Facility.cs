﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SQLite;


namespace BloqCert.Models
{
    public class Facility
    {
        // Manufacturer Information
        // any type of government issued registration id to the manufacturer

        [Column("facilityRegId")]
        [JsonProperty("facilityRegId")]
        public string RegId { get; set; }

        // name of manufacturer
        [Column("facilityName")]
        [JsonProperty("facilityName")]
        public string CompanyName { get; set; }

        [Column("facilityLocation")]
        [JsonProperty("facilityLocation")]
        public string Location { get; set; }

        [Column("facilityContactFirstName")]
        [JsonProperty("facilityContactFirstName")]
        public string ContactFirstName { get; set; }

        [Column("facilityContactLastName")]
        [JsonProperty("facilityContactLastName")]
        public string ContactLastName { get; set; }

        [Column("facilityContactEmail")]
        [JsonProperty("facilityContactEmail")]
        public string ContactEmail { get; set; }

        [Column("facilityContactTelephone")]
        [JsonProperty("facilityContactTelephone")]
        public string ContactTelephone { get; set; }

        [Column("facilityPrimaryStreetAddress")]
        [JsonProperty("facilityPrimaryStreetAddress")]
        public string Address1 { get; set; }

        [Column("facilitySecondaryStreetAddress")]
        [JsonProperty("facilitySecondaryStreetAddress")]
        public string Address2 { get; set; }

        [Column("facilityZipCode")]
        [JsonProperty("facilityZipCode")]
        public string ZipCode { get; set; }

        [Column("facilityCity")]
        [JsonProperty("facilityCity")]
        public string City { get; set; }

        [Column("facilityState")]
        [JsonProperty("facilityState")]
        public string State { get; set; }

        [Column("facilityCountry")]
        [JsonProperty("facilityCountry")]
        public string Country { get; set; }

    }
}
