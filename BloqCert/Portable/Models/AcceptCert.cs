﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace BloqCert.Models
{
    public class AcceptCert
    {

        [JsonProperty("applicationId")]
        public string ApplicationId { get; set; }

        [JsonProperty("expirationDate")]
        public DateTime ExpirationDate { get; set; }

    }

    public class RejectCert
    {

        [JsonProperty("applicationId")]
        public string ApplicationId { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

    }

}
