﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BloqCert.Models
{
    public class APIResult
    {

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("message")]
        public virtual string Message { get; set; }

    }

    public class RegUserResult : APIResult
    {
        [JsonProperty("pass")]
        public string Password { get; set; }
    }

    public class LoginResult : APIResult
    {

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

    }

    public class CertArrayResult : APIResult
    {
        [JsonProperty("data")]
        public List<CertKeyRecord> Data;
    }

    public class CertStatusResult : APIResult
    {
        [JsonProperty("status")]
        public string Status { get; set; }
    }

}
