﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SQLite;


namespace BloqCert.Models
{
    public class Product
    {
        // ProductInformation
        [Column("productName")]
        [JsonProperty("productName")]
        public string ProductName { get; set; }

        [Column("productDescription")]
        [JsonProperty("productDescription")]
        public string ProductDescription { get; set; }

        [Column("productManufacturedDate")]
        [JsonProperty("productManufacturedDate")]
        public DateTime ProductManufacturedDate { get; set; }

        [Column("productExpirationDate")]
        [JsonProperty("productExpirationDate")]
        public DateTime ProductExpirationDate { get; set; }

        [Column("productQuantity")]
        [JsonProperty("productQuantity")]
        public string ProductQuantity { get; set; }

    }

}
