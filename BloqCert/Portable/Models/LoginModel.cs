﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BloqCert.Models
{
    public class LoginModel
    {


        public LoginModel()
        {

        }

        public LoginModel(string username, string password)
        {
            Username = username;
            Password = password;
        }

        [JsonProperty("username")]
        public string Username { get; set; }


        [JsonProperty("password")]
        public string Password { get; set; }

        public static implicit operator LoginModel(BloqCert.Views.LoginScreen.LoginPageViewModel x)
        {
            return new LoginModel { Username = x.Username, Password = x.Password };
        }

        public static explicit operator BloqCert.Views.LoginScreen.LoginPageViewModel(LoginModel y)
        {
            return new Views.LoginScreen.LoginPageViewModel() { Username = y.Username, Password = y.Password };
        }

    }
}
