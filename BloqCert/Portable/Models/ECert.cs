﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloqCert.Models
{
  
    
    // =====================================================================================================================
    //  define eCert class
    // 
    // =====================================================================================================================
    public class ECert
    {
        public ECert()
        {
            AssignNewId();
        }

        public ECert(bool createClasses)
        {
            if (createClasses)
            {

                AssignNewId();

                Shipment = new Shipment();
                Product = new Product();
                Facility = new Facility();
            }
        }

        public void AssignNewId()
        {
            var rnd = new Random((int)(DateTime.Now.ToFileTime() & 0x7fffffff));
            ApplicationId = rnd.Next(1, 0x7ffffffe).ToString();
        }

        // the applicationId (main key for block)
        [Column("applicationId")]
        [JsonProperty("applicationId")]
        public string ApplicationId { get; set; }

        // The exporterId. Uses the information
        // from the export participant
        [Column("exporterId")]
        [JsonProperty("exporterId")]
        public string ExporterId { get; set; }

        [Column("transactionId")]
        [JsonIgnore()]
        public string TransactionId { get; set; }

        //// distinationCountry
        //[Column("destinationCountry")]
        //[JsonProperty("destinationCountry")]
        //public string DestinationCountry { get; set; }

        // comment
        [Column("comment")]
        [JsonProperty("comment")]
        public string Comment { get; set; }

        [Column("certIssueDate")]
        [JsonProperty("certIssueDate")]
        public DateTime CertIssueDate { get; set; }

        [Column("certExpDate")]
        [JsonProperty("certExpDate")]
        public DateTime CertExpDate { get; set; }

        // facility ID
        [Column("facility")]
        [JsonProperty("facility")]
        public Facility Facility { get; set; }

        // productInformation
        [Column("product")]
        [JsonProperty("product")]
        public Product Product { get; set; }

        // Shipment Information
        [Column("shipment")]
        [JsonProperty("shipment")]
        public Shipment Shipment { get; set; }

        [Column("status")]
        [JsonProperty("status")]
        public string Status { get; set; }

    }

}
