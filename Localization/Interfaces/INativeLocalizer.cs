﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloqCert.Localization
{
    public interface INativeLocalizer
    {
        void Localize(string locale);
    }
}
