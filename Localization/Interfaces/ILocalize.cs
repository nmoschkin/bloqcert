﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace BloqCert.Localization
{

    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
    }

}
