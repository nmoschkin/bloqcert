﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using Xamarin.Forms;

namespace BloqCert.Localization.Interfaces
{
    public interface ICultureManager
    {
        FlowDirection FlowDirection { get; }

        void InitLanguage();
        
        CultureInfo ActiveCulture { get; set; }

        void ConfigureAppForCulture(CultureInfo cult);

        void ResetCulture();

    }

}
